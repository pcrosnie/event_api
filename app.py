from fastapi import FastAPI, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from event_api.users.resource import user_login, register_user
from event_api.users.schemas import User, UserRegister
from event_api.database import SessionLocal, Base, engine

Base.metadata.create_all(engine)

app = FastAPI()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")

async def get_current_user(token: str = Depends(oauth2_scheme)):
    user = fake_decode_token(token)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return user

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/")
async def root():
    return {"message": "Hello World"}

# Doesn't work for the moment
@app.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    return user_login(db, form_data)


@app.post("/register")
async def register(user: UserRegister, db: Session = Depends(get_db)):
    user_registered = register_user(db, user)
    if not user_registered:
        raise HTTPException(status_code=400, detail="Not Created")
    return {"message": "User Successfully created."}

# Dependant from login route
@app.get("/users/me")
async def read_user_me(current_user: User = Depends(get_current_user)):
    return current_user


# Events 
@app.post("/event/create")
async def create_event():
    pass

@app.post("/event/subscribe")
async def subscribe_event():
    pass
