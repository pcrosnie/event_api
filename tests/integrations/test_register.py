import requests
from .database import truncate_all

TABLES = [
    "users"
]

def setup_function(_):
    """Executed before each test, start fresh.

    Clear the database content.
    """
    truncate_all(TABLES)


def test_register():
    user_infos = {
        "email": "toto@gmail.com",
        "password": "toto",
        "fullname": "toto la praline"
    }
    response = requests.post("http://localhost:8000/register", json=user_infos)
    assert response.status_code == 200
    response = requests.post("http://localhost:8000/register", json=user_infos)
    assert response.status_code == 400