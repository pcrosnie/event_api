import requests
from .database import truncate_all

TABLES = [
    "users"
]

def setup_function(_):
    """Executed before each test, start fresh.

    Clear the database content.
    """
    truncate_all(TABLES)


def test_login():
    email = "toto@gmail.com"
    password = "toto"
    user_infos = {
        "email": email,
        "password": password,
        "fullname": "toto la praline"
    }
    response = requests.post("http://localhost:8000/register", json=user_infos)
    assert response.status_code == 200

    user_login_infos = {
        "username": email,
        "password": password
    }
    response = requests.post("http://localhost:8000/login", data=user_login_infos)
    from pprint import pprint
    pprint(response.text)
    assert response.status_code == 200
    assert False
