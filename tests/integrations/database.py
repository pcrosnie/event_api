from .config import DB_URI
import psycopg2
from psycopg2 import sql
from psycopg2.extras import DictCursor

def truncate_all(TABLES_USED_IN_TEST):
    """
        Truncates db's tables before tests
    """
    truncate_all_statement = sql.SQL("TRUNCATE TABLE {} CASCADE").format(
        sql.SQL(', ').join(
            map(sql.Identifier, TABLES_USED_IN_TEST)
        )
    )

    connection = psycopg2.connect(DB_URI)
    connection.autocommit = True
    cursor = connection.cursor()
    cursor.execute(truncate_all_statement)
    cursor.close()
    connection.close()