.PHONY: build test

build:
	docker build -t registry.gitlab.com/pcrosnie/event_api .

test:
	docker-compose exec event_api py.test tests/integrations/