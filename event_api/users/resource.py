from typing import Optional

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from .schemas import User, UserInDB, UserRegister
from ..security import hash_password

from .models import UserDB


def get_user(db, username: str):
    if username in db:
        user_dict = db[username]
        return UserInDB(**user_dict)


def fake_decode_token(token):
    # This doesn't provide any security at all
    # Check the next version
    user = get_user(fake_users_db, token)
    return user

def register_user(db: Session, user: UserRegister):
    existing = db.query(UserDB).filter(UserDB.email == user.email).scalar()
    if existing:
        raise HTTPException(status_code=400, detail="User Already Exists.")

    db_user = UserDB(email=user.email, full_name=user.fullname, hashed_password=hash_password(user.password))

    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def user_login(db, form_data: OAuth2PasswordRequestForm, token):
    # To replace with real db calls
    print("ICI")
    user_dict = db.query(UserDB).filter(UserDB.email == form_data.username).one()
    print(user_dict)
    print("LA")
    if not user_dict:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    hashed_password = hash_password(form_data.password)
    if not hashed_password == user_dict.hashed_password:
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    return {"access_token": token, "token_type": "bearer"}