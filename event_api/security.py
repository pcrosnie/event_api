import hashlib
import os

salt = bytes("`s89auj~9q0qa0)kqloq0a{", 'utf-8')

def hash_password(password):
    key = hashlib.pbkdf2_hmac(
        'sha256', # The hash digest algorithm for HMAC
        password.encode('utf-8'), # Convert the password to bytes
        salt, # Provide the salt
        100000, # It is recommended to use at least 100,000 iterations of SHA-256 
        dklen=128
    )
